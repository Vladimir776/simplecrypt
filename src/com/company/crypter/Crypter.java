package com.company.crypter;

import com.company.exception.CryptErrorException;

public interface Crypter {

    void startCrypt (byte buffer[] , byte password[]) throws CryptErrorException;
}