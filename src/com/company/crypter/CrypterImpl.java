package com.company.crypter;

import com.company.exception.CryptErrorException;

import java.util.Arrays;

public class CrypterImpl implements Crypter {
    @Override
    public void startCrypt(byte[] buffer, byte[] password) throws CryptErrorException {
        for (int i = 0; i < buffer.length; i = i + 8){
            byte segment[] = Arrays.copyOfRange(buffer, i, i + 8);
            System.arraycopy(buffer, i, segment, 0, 8);
            try {
                ProcessData(password, segment, segment);
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
                throw new CryptErrorException("Error : Failed to crypt/decrypt file");
            }
            System.arraycopy(segment, 0, buffer, i, 8);
        }
    }

    public static void ProcessData( final byte[] password, final byte[] data, byte[] result )
    {
        final int ARG_LEN = 8;
        if( (data.length != ARG_LEN) || (password.length != ARG_LEN) || (result.length != ARG_LEN) )
        {
            throw new RuntimeException( "Wrong len" );
        }

        for( int i = 0; i < 8; ++i )
        {
            result[i] = (byte)(data[i] ^ password[i]);
        }
    }
}
