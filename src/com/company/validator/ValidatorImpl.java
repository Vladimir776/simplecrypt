package com.company.validator;

import com.company.enumorder.EnumOrder;

import java.io.File;
import java.util.regex.Pattern;

public class ValidatorImpl implements Validator {
    private static String info = "usage: Hello ! This is SimpleCrypt by V.Kazarcev." + "\n" + "Please enter: Input File | Output File | Password | Crypt Type "
            + "\n" + "Lenght of password must be 8 symbols " + "\n" + "Crypt command : decrypt | encrypt"+ "\n" + "Encrypting file must have .crypt expansion";
    private final static short ARGS_SIZE = 4;
    private String inFile;
    private String outFile;
    private String cryptType;
    private final int argSize;
    private long inputFileSize;
    private final String data[];

    public ValidatorImpl(String data[]) {
        this.data = data;
        this.argSize = data.length;
    }

    public boolean validate(){

        return validateArgs() ;
    }
    private   boolean validateArgs( )
    {
        if (argSize == 0 ) {
            System.out.println(info);
            return false;
        }
        this.inFile = data[EnumOrder.INPUT_FILE.getPosition()];
        this.outFile = data[EnumOrder.OUTPUT_FILE.getPosition()];
        this.cryptType = data[EnumOrder.CRYPT_TYPE.getPosition()];
        if (ARGS_SIZE != argSize) {
            System.out.println("Error : Invalid Number of args");
            return false;
        }
        return validateFiles();
    }


    private  boolean validateFiles( )
    {
        File testingFile =  new File(inFile);
        if (!testingFile.exists()) {
            System.out.println("Error : file dose not exist.");
            return false;
        }
        this.inputFileSize = testingFile.length();
        if (inFile.equals(outFile)) {
            System.out.println("Error : In and out file have same name.");
            return false;
        }
        if (inputFileSize == 0) {
            System.out.println("Error : The input file length = 0 bytes");
            return false;
        }
        return validateCrypt();
    }

    private  boolean validateCrypt( )
    {
        Pattern pattern = Pattern.compile(".+\\.crypt");
        boolean validate = false;
        switch (cryptType) {
            case ("decrypt"):
                if(pattern.matcher(inFile).find() && !pattern.matcher(outFile).find()) {
                    validate = true;
                } else {
                    System.out.println("Error : Wrong decrypt params, see usage.");
                }
                break;
            case ("encrypt"):
                if(!pattern.matcher(inFile).find() && pattern.matcher(outFile).find() ) {
                    validate = true;
                } else {
                    System.out.println("Error : Wrong encrypt params, see usage");
                }
                break;
            default:
                System.out.println("Error : Wrong crypt command. See usage.");
                break;
        }
        return validate;
    }
}
