package com.company.exception;

public class CryptErrorException extends RuntimeException {
    public CryptErrorException() {
        super();
    }

    public CryptErrorException(String message) {
        super(message);
    }

    public CryptErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
