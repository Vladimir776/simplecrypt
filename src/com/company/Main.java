package com.company;

import com.company.crypter.CrypterImpl;
import com.company.utilclass.FileStreamHelper;
import com.company.validator.Validator;
import com.company.validator.ValidatorImpl;

public class Main {

    public static void main(String[] args) {
        Validator validator = new ValidatorImpl(args);

        if (validator.validate()) {
            FileStreamHelper fileStreamHelper = new FileStreamHelper(new CrypterImpl());
            fileStreamHelper.startCrypt(args);
        }

    }

    }
