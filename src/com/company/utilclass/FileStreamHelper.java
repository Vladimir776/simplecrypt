package com.company.utilclass;

import com.company.crypter.Crypter;
import com.company.enumorder.EnumOrder;
import com.company.exception.CryptErrorException;

import java.io.*;

public class FileStreamHelper {
    private Crypter crypter;
    public static int BUFFER_SIZE = 64000;
    public FileStreamHelper(Crypter crypter ) {
        this.crypter = crypter;
    }
    public void startCrypt (String data[]) {
        String inFile = data[EnumOrder.INPUT_FILE.getPosition()];
        String outFile = data[EnumOrder.OUTPUT_FILE.getPosition()];
        byte password[] = data[EnumOrder.PASSWORD.getPosition()].getBytes();
        byte buffer[] = new byte[BUFFER_SIZE];
        final long fileSize = new File(inFile).length();
        long sizeCounter = 0;
        String result = "  Success";
        try (FileInputStream inputStream = new FileInputStream(inFile); FileOutputStream outputStream = new FileOutputStream(outFile); ) {
            while(true) {
                if(inputStream.read(buffer, 0, BUFFER_SIZE) == -1 ) {
                    break;
                }
                try {
                    crypter.startCrypt(buffer, password);
                } catch (CryptErrorException e) {
                    System.out.println(e.getMessage());
                    result = "  Failed";
                    break;
                }
                if (sizeCounter + BUFFER_SIZE > fileSize){
                    outputStream.write(buffer, 0, (int)(fileSize - sizeCounter));
                } else {
                    outputStream.write(buffer, 0, BUFFER_SIZE);
                }
                sizeCounter += BUFFER_SIZE;
            }
            System.out.println(data[EnumOrder.CRYPT_TYPE.getPosition()] + result);
        } catch (FileNotFoundException e) {
            System.out.println(  e.getMessage() );
        } catch (IOException e) {
            System.out.println( e.getMessage() );
        }
    }
}
