package com.company.enumorder;

public enum EnumOrder {
    INPUT_FILE(0) , OUTPUT_FILE(1) , PASSWORD(2) , CRYPT_TYPE(3);

    EnumOrder(int position) {
        this.position = position;
    }

    private final int position;

    public int getPosition() {
        return position;
    }
}
